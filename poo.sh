#!/bin/bash

root=.
app=app

[[ "$1" != "" ]] && {
    app="$1"
}

lessc=lessc

[[ -f /usr/local/bin/lessc ]] && {
    lessc=/usr/local/bin/lessc
}

echo "lessc=$lessc"
$lessc -v || {
    echo "Fail"
    exit 1
}

[[ ! -d "$root" ]] && { echo "$root is not a director"; exit 1; }

declare -A map

while read file; do
    if [[ "$file" != *public/assets/* ]]; then
        map["$file"]=0
    fi
done < <(find "$root" -type f -iname "*.coffee" -or -iname "$app.less" -or -iname "$app.sass" -or -iname "$app.scss")

function build() {
    file="$1"
    echo "Build $file ..."
    basename="$(basename "$file")"
    path="$(dirname "$file")"
    name="${basename%.*}"
    type="${basename##*.}"
    case "$type" in
    less)
        if [[ "$name" == "app" || "$name" == "site" || "$name" == "$app" ]]; then
            time $lessc -x --source-map "$file" "$path/../css/${name}.css"
            du -hs "$path/../css/${name}.css"
        else
            time $lessc -x --source-map "$file" "$path/${name}.css"
            du -hs "$path/${name}.css"
        fi
        if [[ $? != 0 ]]; then
            local grep=$($lessc --no-color -x "$file" /dev/null 2>&1 | grep " on line ")
            local src="$(echo "$grep" | cut -d ',' -f1)"
            local line="$(echo "$grep" | cut -d ',' -f1)"
            src="${src% on line*}"
            src="/$(echo "$src" | cut -d '/' -f2-)"
            echo "src=$src"
            #src="$(echo "$src" | rev | cut -d ' ' -f4 | rev)"
            #src="$(echo "$src" | grep '^home$')"
            line="${line##* }"
            echo "subl: $src:$line"
            if [[ "$line" != "" ]]; then
                subl "$src:$line"
            else
                subl "$src"
            fi
        fi
        ;;
    coffee)
        time coffee -c "$file"
        ;;
    sass)
        time sass "$file" > "$path/${name}.css"
        ;;
    *)
        echo "Fail: $file"
        exit 1
    esac
}

while true; do
    for file in "${!map[@]}"; do
        old="${map["$file"]}"
        stat="$(stat "$file" -c %Y)"
        if [[ $old < $stat ]]; then
            map["$file"]=$stat
            build "$file"
        fi
        stat="$(stat "$(dirname "$file")" -c %Y)"
        if [[ $old < $stat ]]; then
            map["$file"]=$stat
            build "$file"
        fi
        dir="$(dirname "$file")/include"
        if [[ -d "$dir" ]]; then
            stat="$(stat "$dir" -c %Y)"
            if [[ $old < $stat ]]; then
                map["$file"]=$stat
                build "$file"
            fi
        fi
    done
    sleep 1
done
